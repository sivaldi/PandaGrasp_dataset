# PandaGrasp Dataset

This page is the official entry point for the repository of PandaGrasp.

PandaGrasp is a real robot dataset that contains sequences of images of a Franka robot's workspace during manipulation and grasping of variety of rigid/soft objects in heaps. The robot's actions on its workspace are driven by semantic actions, defined per our hierarchy. The images are frames captured by a camera placed on top of the workspace and partially capturing the robot's end-effector. 

The dataset is meant to be used for visual prediction for robot learning and manipulation/grasping.

## Download and setup dataset

Please download PandaGrasp through this [link](https://mybox.inria.fr/smart-link/6a84dc56-0b12-4d18-8092-368fe366b7d8/) and unzip the package.

Install dependencies:
```
pip install -r requirements.txt
```

## View dataset
To inspect the content of the dataset, you can write a script as follows:
```
from panda_grasp import PandaGrasp
dataset = PandaGrasp(batch_size=32, data_dir='PandaGrasp', metadata='metadata.pkl')
dataset_iter = iter(dataset)
x = next(dataset_iter)
image = x['images'][1]
action = x['actions'][1]
state = x['states'][1]
```

### Example:
<img src='example.gif' align="left" width=800>
<br><br><br>

# Setup for data collection
The robot setup consists of a Franka Emika Panda robot, mounted on a tabletop, and equipped with a standard gripper. An Intel RealSense camera is used to capture the robot's end-effector and its workspace.
<img src='setup.png' align="left" width=500>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br>

## Use the dataset

The dataset is suitable for learning visual prediction models.  For example, we used it [to train the visual prediction models of VP-GO.](https://gitlab.inria.fr/sivaldi/HEAP-VP-GO)

<br><br><br><br><br><br><br><br><br><br><br><br><br><br>

# Acknowledgments

HEAP project 

