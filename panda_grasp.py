import os
import copy
import h5py
import random
import numpy as np
import pandas as pd
import multiprocessing
from easydict import EasyDict
from collections import OrderedDict

def load_fn(inputs):
    f_name, file_metadata, hparams, rng = inputs
    rng = random.Random(rng)
    assert os.path.exists(f_name) and os.path.isfile(f_name), 'invalid f_name'
    start_time, n_states = 0, min(file_metadata['T'])
    assert n_states > 1, 'must be more than one state in loaded tensor!'
    assert 1 < hparams.load_T <= n_states
    start_time = rng.randint(0, n_states - hparams.load_T)
    n_states = hparams.load_T
    with h5py.File(f_name, 'r') as hf:
        images = hf['images{}'.format(hparams.img_size)][()][start_time: start_time+n_states][None]
        states = hf['states'][()][start_time: start_time+n_states]
        actions = hf['actions'][()][start_time: start_time+n_states-1]
    images = (images / 255.0).astype(np.float32)
    images = np.squeeze(images)

    fetch = OrderedDict()
    fetch['images'] = images
    fetch['states'] = states
    fetch['actions'] = actions
    
    return fetch

class PandaGrasp(object):
    def __init__(self, batch_size, data_dir, metadata, hparams=dict()):
        assert isinstance(batch_size, int), "batch_size must be an integer"
        self._batch_size = batch_size
        self._data_dir = data_dir

        if isinstance(metadata, str):
            self._metadata = [pd.read_pickle(data_dir + '/'+ metadata)]
        elif isinstance(metadata, list):
            self._metadata = [pd.read_pickle(data_dir + '/'+ m) for m in metadata]
        else:
            raise NotImplementedError

        self._hparams = self._get_default_hparams()
        for k, v in hparams.items():
            assert k in self._hparams
            self._hparams[k] = v

        self._init_rng()        
        self._init_dataset()
    
    def _init_rng(self):
        self._random_generator = {}
        
        seeds = [None for _ in range(len(self.modes) + 1)]
        if self._hparams.RNG:
            seeds = [i + self._hparams.RNG for i in range(len(seeds))]
        
        for k, seed in zip(self.modes + ['base'], seeds):
            if k == 'train' and self._hparams.use_random_train_seed:
                seed = None
            self._random_generator[k] = random.Random(seed)
        self._np_rng = np.random.RandomState(self._random_generator['base'].getrandbits(32))

    def _init_dataset(self):
        assert self._hparams.load_T >=0, 'load_T should be non-negative!'
        min_steps = min([min(m['T']) for m in self._metadata])
        if not self._hparams.load_T:
            self._hparams.load_T = min_steps
        else:
            assert self._hparams.load_T <= min_steps, 'ask to load {} steps but some records only have {}!'.format(self._hparams.min_T, min_steps)

        if self._hparams.proportion:
            self.proportion = [len(m)/sum([len(m) for m in self._metadata]) for m in self._metadata]
        else:
            self.proportion = [1/len(self._metadata) for m in self._metadata]

        mode_sources = [[] for _ in range(len(self.modes))]
        for i, metadata in enumerate(self._metadata):
            for j, (m, m_s) in enumerate(zip(self.modes, mode_sources)):
                fps = copy.deepcopy(list(metadata[metadata['set'] == m]['file']))
                if len(fps):
                    self._random_generator['base'].shuffle(fps)
                    m_s.append((fps, metadata[metadata['set'] == m]))

        self.mode_sources = dict()
        for m, m_s in zip(self.modes, mode_sources):
            self.mode_sources[m] = m_s

        self._mode_generators = {}
        for mode in self.modes:
            self._mode_generators[mode] = iter(self._hdf5_generator(mode))

        self._mode = 'train'

        if self._hparams.multi_multiprocess:
            self._n_workers = min(self._batch_size, multiprocessing.cpu_count())
            if self._hparams.pool_workers:
                self._n_workers = min(self._hparams.pool_workers, multiprocessing.cpu_count())
            self._pool = multiprocessing.Pool(self._n_workers)

    def __iter__(self):
        return self

    def __next__(self):
        inputs = next(self._mode_generators[self._mode])
        return inputs    

    def set_mode(self, mode):
        self._mode = mode

    def _hdf5_generator(self, mode):
        file_indices, source_epochs = [[0 for _ in range(len(self.mode_sources[mode]))] for _ in range(2)]
        rng = np.random.RandomState(self._random_generator[mode].getrandbits(32))
        while True:
            file_hparams = [copy.deepcopy(self._hparams) for _ in range(self._batch_size)]
            if self._hparams.RNG:
                file_rng = [self._random_generator[mode].getrandbits(64) for _ in range(self._batch_size)]
            else:
                file_rng = [None for _ in range(self._batch_size)]
           
            file_names, file_metadata = [], []
            while len(file_names) < self._batch_size:
                if len(file_names) == 0 and len(file_names) + len(file_indices) <= self._batch_size:
                    selected_source = self._np_rng.choice(len(file_indices), len(file_indices), replace=False)
                else:
                    selected_source = rng.choice(len(file_indices), 1, replace=False, p=self.proportion)
            
                for ss in selected_source:
                    selected_file = self.mode_sources[mode][ss][0][file_indices[ss]]
                    file_indices[ss] += 1
                    selected_file_metadata = self.mode_sources[mode][ss][1][self.mode_sources[mode][ss][1]['file'] == selected_file]
                    file_names.append(selected_file)
                    file_metadata.append(selected_file_metadata)
                    
                    if file_indices[ss] >= len(self.mode_sources[mode][ss][0]):
                        file_indices[ss] = 0
                        source_epochs[ss] += 1
                        rng.shuffle(self.mode_sources[mode][ss][0])

            batch_jobs = [['{}/{}/{}'.format(self._data_dir, np.array(fm['mode'])[0], fn), fm, fh, fr] for fn, fm, fh, fr in zip(file_names, file_metadata, file_hparams, file_rng)]

            if self._hparams.multi_multiprocess == True:
                try:
                    batches = self._pool.map_async(load_fn, batch_jobs).get(timeout=10)
                except:
                    batches = []
                    for inputs in batch_jobs:
                        batches.append(load_fn(inputs))
            else:
                batches = []
                for inputs in batch_jobs:
                    batches.append(load_fn(inputs))
            
            fetch = OrderedDict([(key, np.concatenate([b[key][None] for b in batches])) for key in batches[0].keys()])
            
            yield fetch

    @staticmethod
    def _get_default_hparams():
        default_dict = {
            'RNG': 11381294392481135266,
            'use_random_train_seed': False,
            'pool_workers': 0,
            'multi_multiprocess': False,
            'load_T': 25,
            'proportion': True,
            'img_size':64
        }        
        return EasyDict(default_dict)
      
    def __contains__(self, item):
        raise NotImplementedError

    @property
    def batch_size(self):
        return self._batch_size

    @property
    def hparams(self):
        return copy.deepcopy(self._hparams)

    @property
    def modes(self):
        return ['train', 'val', 'test']

if __name__ == '__main__':
    dataset = PandaGrasp(batch_size=32, data_dir='/home/ama/anjistorage/Dataset/PandaGrasp', metadata='metadata.pkl')
    dataset_iter = iter(dataset)
    x = next(dataset_iter)
    image = x['images'][1]
    action = x['actions'][1]
    state = x['states'][1]


    stop = 0
